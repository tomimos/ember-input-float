import Component from '@ember/component';

export default Component.extend({
  init() {
    this._super(...arguments);
    this.send('roundInput');
  },
  tagName: '',
  nullable: false,
  actions:{
    roundInput(){
      if((this.get("value") !== "" && this.get("value") !== null) || this.get("nullable") === false){
        var rounded = (Math.round( this.get('value') * 100 ) / 100).toFixed(2);
        this.setProperties({'value': rounded});
        this.rerender();
      }
      else{
        this.setProperties({'value': ""});
      }

    },
    focusIn(){
      this.send('roundInput');
      if(this.get('focus-in')){
        this.get('focus-in')()
      }
    },
    focusOut(){
      this.send('roundInput');
      if(this.get('focus-out')){
        this.get('focus-out')()
      }
    }
  }
});
